. /etc/bashrc
export EDITOR='vim'
export TERM='xterm-256color'
alias tmux='tmux -2'
# Disable CTRL-S Freeze
stty -ixon

