#set-option -g bell-action any
#set-option -g visual-activity on
#set-window-option -g monitor-activity on

set -g default-shell "/usr/local/bin/fish"

set-option -g history-limit 10000

unbind-key C-b
set -g prefix C-b
set -g prefix2 `
# bind-key -n C-q send-prefix # nested tmux
bind-key b send-prefix
bind-key ` send-prefix
bind-key C-b last-window
bind-key e previous-window
unbind %
bind-key v split-window -h
bind-key s split-window -v
bind-key - copy-mode
bind-key g choose-tree
bind-key C-r rotate-window
bind-key C-l clear-history

# Use vim keybindings in copy mode
setw -g mode-keys vi
# Setup 'v' to begin selection as in Vim
bind-key -t vi-copy v begin-selection
unbind -t vi-copy Enter

# Mac Clipboard Sync - requires 'brew install reattach-to-user-namespace'
# bind-key -t vi-copy y copy-pipe "reattach-to-user-namespace pbcopy"
# bind-key -t vi-copy Enter copy-pipe "reattach-to-user-namespace pbcopy"
# bind \ run "reattach-to-user-namespace pbpaste | tmux load-buffer - && tmux paste-buffer"

# Linux Clipboard Sync
bind-key -t vi-copy y copy-pipe "xsel -i -b"
bind-key -t vi-copy Enter copy-pipe "xsel -i -b"
bind \ run "xsel -o -b | tmux load-buffer - && tmux paste-buffer" # Paste
# bind-key -t vi-copy y copy-pipe "xclip -i -selection clipboard"
# bind-key -t vi-copy Enter copy-pipe "xclip -i -selection clipboard"
# bind \ run "xclip -o | tmux load-buffer - && tmux paste-buffer" # Paste

bind-key -t vi-copy o copy-pipe "xargs google-chrome"

# # Smart pane switching with awareness of vim splits
is_vim='echo "#{pane_current_command}" | grep -iqE "(^|\/)g?(view|.*emacs.*|n?vim?)(diff)?$"'
bind -n M-h if-shell "$is_vim" "send-keys M-h" "select-pane -L"
bind -n M-n if-shell "$is_vim" "send-keys M-n" "select-pane -D"
bind -n M-e if-shell "$is_vim" "send-keys M-e" "select-pane -U"
bind -n M-l if-shell "$is_vim" "send-keys M-l" "select-pane -R"
bind -n M-\ if-shell "$is_vim" "send-keys M-\\" "select-pane -l"

set -g mode-keys vi
set -g mode-mouse on
set -g mouse-resize-pane on
set -g mouse-select-pane on
set -g mouse-select-window on
set -g mouse-utf8 on

set -g base-index 1

#set-window-option -g alternate-screen on
set -g default-terminal "screen-256color"

# Allow pasteboard access in OSX
if-shell 'test "$(uname -s)" = Darwin' 'set-option -g default-command "exec reattach-to-user-namespace -l zsh"'

set-option -g status on
set-option -g status-interval 2
set-option -g status-utf8 on
set-option -g utf8 on
set-option -g status-justify "left"
set-option -g status-left-length 60
set-option -g status-right-length 90
set-option -g visual-activity on
# set-option -g visual-content on

# Custom colors!
source-file "$HOME/dotfiles/tmux/colors-purple.conf"

set -g escape-time 0
