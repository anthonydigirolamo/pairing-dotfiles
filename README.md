# dotfiles for remote tmux/vim pairing

## Initial Setup

    cd ~
    git clone --recursive https://gitlab.com/anthonydigirolamo/pairing-dotfiles.git dotfiles
    cd dotfiles
    ./mksymlinks

## EC2 Box Setup

### Yum Packages

    sudo yum install ack vim openssl-devel ruby-rake ruby-irb ruby ncurses-devel make automake gcc gcc-c++ kernel-devel tmux git mosh

### Fish Setup

    curl -O https://fishshell.com/files/2.5.0/fish-2.5.0.tar.gz
    cd fish-2.5.0
    make
    sudo make install

